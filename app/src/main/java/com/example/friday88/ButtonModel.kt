package com.example.friday88

data class ButtonModel(
    val text: String?,
    val icon: Int? = null
)
