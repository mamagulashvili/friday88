package com.example.friday88

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.friday88.databinding.ImageButtonItemBinding
import com.example.friday88.databinding.NumberItemBinding

typealias onButtonClick = (position: Int) -> Unit

class ButtonAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val itemList = mutableListOf<ButtonModel>()
    override fun getItemCount(): Int = itemList.size
    lateinit var onButtonClick: onButtonClick

    companion object {
        private const val NUMBER_BTN = 0
        private const val IMAGE_BTN = 1
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolder -> holder.onBind()
            is IconViewHolder -> holder.onBind()
        }
    }

    inner class ViewHolder(val binding: NumberItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            when (adapterPosition) {
                0 -> binding.root.text = "1"
                1 -> binding.root.text = "2"
                2 -> binding.root.text = "3"
                3 -> binding.root.text = "4"
                4 -> binding.root.text = "5"
                5 -> binding.root.text = "6"
                6 -> binding.root.text = "7"
                7 -> binding.root.text = "8"
                8 -> binding.root.text = "9"
                10 -> binding.root.text = "0"
            }
            binding.root.setOnClickListener {
                onButtonClick.invoke(adapterPosition)
            }
        }
    }

    inner class IconViewHolder(val binding: ImageButtonItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun onBind() {
            when (adapterPosition) {
                11 -> binding.root.setImageResource(R.drawable.ic_remove)
                9 -> binding.root.setImageResource(R.drawable.ic_touch__id)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            NUMBER_BTN -> return ViewHolder(
                NumberItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            else -> return IconViewHolder(
                ImageButtonItemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        val model = itemList[position]
        return if (model.icon != null)
            IMAGE_BTN
        else
            NUMBER_BTN
    }

    fun setData(newList: MutableList<ButtonModel>) {
        this.itemList.apply {
            clear()
            addAll(newList)
        }
        notifyDataSetChanged()
    }
}