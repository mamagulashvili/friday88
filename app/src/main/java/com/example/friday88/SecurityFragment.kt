package com.example.friday88

import android.graphics.Color
import android.os.Bundle
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.friday88.databinding.FragmentSecurityBinding

class SecurityFragment : Fragment() {
    private var _binding: FragmentSecurityBinding? = null
    private val binding get() = _binding!!
    private val buttonAdapter: ButtonAdapter by lazy { ButtonAdapter() }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSecurityBinding.inflate(layoutInflater, container, false)
        init()
        return binding.root
    }

    private fun init() {
        initRecycle()
    }

    private fun initRecycle() {
        binding.rvNumbers.apply {
            layoutManager = GridLayoutManager(requireContext(), 3)
            adapter = buttonAdapter
        }
        setData()
        setOnButtonClick()
    }

    private fun setOnButtonClick() {
        val passList = mutableListOf<Int>()
        var click = 0
        buttonAdapter.onButtonClick = {
            click++
            if (it == 10){
                passList.add(0)
            }else{
                passList.add(it + 1)
            }

            when (click) {
                1, 5 -> {
                    binding.btnGreenOne.setBackgroundColor(Color.GREEN)
                }
                2, 6 -> {
                    binding.btnGreenTwo.setBackgroundColor(Color.GREEN)
                }
                3, 7 -> {
                    binding.btnGreenThree.setBackgroundColor(Color.GREEN)
                }
                4, 8 -> {
                    binding.btnGreenFour.setBackgroundColor(Color.GREEN)
                }
            }
            d("LISTT", "$passList")
            if (passList.size == 4) {
                binding.txtPassword.text = "Repeat Password"
                binding.apply {
                    btnGreenOne.setBg( android.R.color.transparent)
                    btnGreenTwo.setBg( android.R.color.transparent)
                    btnGreenThree.setBg( android.R.color.transparent)
                    btnGreenFour.setBg( android.R.color.transparent)
                }
            }
            for (i in 0 until passList.size-7) {
                if (passList[i] + passList[i + 1] + passList[i + 2] + passList[i + 3] == passList[i + 4] + passList[i + 5] + passList[i + 6] + passList[i + 7]){
                    Toast.makeText(requireContext(), "successfully", Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(requireContext(), "error", Toast.LENGTH_SHORT).show()
                    binding.apply {
                        btnGreenOne.setBg(android.R.color.holo_red_light)
                        btnGreenTwo.setBg(android.R.color.holo_red_light)
                        btnGreenThree.setBg(android.R.color.holo_red_light)
                        btnGreenFour.setBg(android.R.color.holo_red_light)
                    }
                }
            }
          when(it){
              10 -> {
                  passList.removeAt(passList.size -1)
                  when(passList.size){
                      1 -> binding.btnGreenOne.setBackgroundColor( ContextCompat.getColor(
                          binding.root.context,
                          android.R.color.holo_red_light
                      ))
                      2 -> binding.btnGreenTwo.setBackgroundColor( ContextCompat.getColor(
                          binding.root.context,
                          android.R.color.holo_red_light
                      ))
                     3 -> binding.btnGreenThree.setBackgroundColor( ContextCompat.getColor(
                          binding.root.context,
                          android.R.color.holo_red_light
                      ))
                      4 -> binding.btnGreenFour.setBackgroundColor( ContextCompat.getColor(
                          binding.root.context,
                          android.R.color.holo_red_light
                      ))
                  }
              }
              9 -> {
                  Toast.makeText(requireContext(), "this is finger print", Toast.LENGTH_SHORT).show()
              }
          }
        }

    }

    private fun setData() {
        val itemList = mutableListOf<ButtonModel>()
        buttonAdapter.setData(
            itemList.apply {
                add(ButtonModel("1"))
                add(ButtonModel("2"))
                add(ButtonModel("3"))
                add(ButtonModel("4"))
                add(ButtonModel("5"))
                add(ButtonModel("6"))
                add(ButtonModel("7"))
                add(ButtonModel("8"))
                add(ButtonModel("9"))
                add(ButtonModel(null, R.drawable.ic_touch__id))
                add(ButtonModel("0"))
                add(ButtonModel(null, R.drawable.ic_remove))

            }
        )
    }
}