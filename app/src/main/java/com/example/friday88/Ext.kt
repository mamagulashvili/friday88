package com.example.friday88

import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat

fun AppCompatButton.setBg(color: Int) {
    this.setBackgroundColor(ContextCompat.getColor(this.context, color))
}